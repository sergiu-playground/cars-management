package com.test.cars.exception;

import org.springframework.http.HttpStatus;

public class VehicleException extends CarManagementException {

	public VehicleException(String messageKey, HttpStatus status, String... arguments) {
		super(messageKey, status, arguments);
	}

	public VehicleException(String messageKey, String... arguments) {
		super(messageKey, arguments);
	}

	public VehicleException(String messageKey) {
		super(messageKey);
	}
	
}