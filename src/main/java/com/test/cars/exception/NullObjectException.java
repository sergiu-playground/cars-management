package com.test.cars.exception;

public class NullObjectException extends Exception{

	public NullObjectException(String message) {
		super(message);
	}

	
}