package com.test.cars.exception;

import org.springframework.http.HttpStatus;

public class ContractException extends CarManagementException {

    public ContractException(String messageKey) {
        super(messageKey);
    }

    public ContractException(String messageKey, String... arguments) {
        super(messageKey, arguments);
    }

    public ContractException(String messageKey, HttpStatus status, String... arguments) {
        super(messageKey, status, arguments);
    }
}
