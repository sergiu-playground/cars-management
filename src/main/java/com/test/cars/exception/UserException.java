package com.test.cars.exception;

import org.springframework.http.HttpStatus;

public class UserException extends CarManagementException {

	public UserException(String messageKey, HttpStatus status, String... arguments) {
		super(messageKey, status, arguments);
	}

	public UserException(String messageKey, String... arguments) {
		super(messageKey, arguments);
	}

	public UserException(String messageKey) {
		super(messageKey);
	}

}