package com.test.cars.utils;

import org.springframework.http.HttpStatus;

import java.io.Serializable;

public class ResponseBuilder implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5113072778400018088L;
	private Boolean success = false;
	private Object data;
	private String[] error;
	private HttpStatus statusCode;

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String[] getError() {
		return error;
	}

	public void setError(String... error) {
		this.error = error;
	}

	public HttpStatus getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(HttpStatus statusCode) {
		this.statusCode = statusCode;
	}

}