package com.test.cars.utils;

public class Constants {

    public static final String NULL_OBJECT = "Null object.";
    public static final String INVALID_CREDENTIALS = "Invalid credentials.";

    private Constants() {
    }
}
