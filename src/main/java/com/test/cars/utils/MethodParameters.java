package com.test.cars.utils;

import com.test.cars.exception.NullObjectException;

public class MethodParameters {

	public static void testIfTheMethodAtributesAreNotNull(Object... objects) throws NullObjectException {

		for (Object parameter : objects) {
			if (parameter == null) {
				throw new NullObjectException("There is a null object in method parameters.");
			}
		}

	}

}