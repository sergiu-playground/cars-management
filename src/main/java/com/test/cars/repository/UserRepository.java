package com.test.cars.repository;

import com.test.cars.domain.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<User, String>{
	
	User findByEmail(final String email);
	
	User findByUsername(final String username);
	
	User findByCNP(final String CNP);
}