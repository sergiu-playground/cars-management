package com.test.cars.service;

import com.test.cars.domain.Vehicle;
import com.test.cars.domain.dto.VehicleDTO;

import java.util.List;

/***
 * 
 * @author sergiu.orian
 *
 */
public interface VehicleService {

	/***
	 * 
	 * @param vehicleDTO
	 * @return
	 */
	Vehicle create(final VehicleDTO vehicleDTO);

	/***
	 * 
	 * @param vehicle
	 * @return
	 */
	Vehicle save(final Vehicle vehicle);

	/***
	 * 
	 * @param id
	 * @return
	 */
	String deleteById(final String id);

	/***
	 * 
	 * @param vehicle
	 * @return
	 */
	Vehicle update(final Vehicle vehicle);

	/***
	 * 
	 * @param id
	 * @return
	 */
	Vehicle findById(final String id);

	/***
	 * 
	 * @return
	 */
	List<Vehicle> findAll();

}