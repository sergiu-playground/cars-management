package com.test.cars.service;

import com.test.cars.domain.dto.UserLoginDTO;
import com.test.cars.exception.NullObjectException;
import com.test.cars.exception.UserException;
import com.test.cars.security.CustomUserDetailsService;
import com.test.cars.utils.MethodParameters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import static com.test.cars.utils.Constants.INVALID_CREDENTIALS;
import static com.test.cars.utils.Constants.NULL_OBJECT;

@Service
public class LoginServiceImpl implements LoginService {

	@Autowired
	private CustomUserDetailsService userDetailsService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public String login(UserLoginDTO userLoginDTO) {


		UserDetails user = null;

		try {
			MethodParameters.testIfTheMethodAtributesAreNotNull(userLoginDTO);
			user = userDetailsService.loadUserByUsername(userLoginDTO.getUsername());
		} catch (NullObjectException e) {
			throw new UserException(NULL_OBJECT, HttpStatus.BAD_REQUEST, e.getMessage());
		}

		if (passwordEncoder.matches(userLoginDTO.getPassword(), user.getPassword())) {
			SecurityContext securityContext = SecurityContextHolder.getContext();
			Authentication authentication = new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
			securityContext.setAuthentication(authentication);
		}else {

			throw new UserException(INVALID_CREDENTIALS, HttpStatus.NOT_FOUND, "Username or password is incorrect.");
		}
		
		return user.getUsername();

	}

	@Override
	public void logout() {
		SecurityContextHolder.clearContext();
	}

}
