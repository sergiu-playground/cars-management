package com.test.cars.service;

import com.test.cars.domain.User;
import com.test.cars.domain.dto.ForgotPasswordDTO;
import com.test.cars.exception.UserException;

/***
 * The service for register flows.
 */
public interface RegistrationService {

    /***
     *
     * @param forgotPasswordDTO Forgot Password Data transfer object.
     * @return the {@link User} id
     * @throws UserException with
     */
    String changePassword(final ForgotPasswordDTO forgotPasswordDTO);
}
