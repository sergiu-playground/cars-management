package com.test.cars.service;

import com.test.cars.domain.Contract;
import com.test.cars.domain.dto.ContractDTO;
import com.test.cars.domain.dto.ConvertersDTO;
import com.test.cars.exception.ContractException;
import com.test.cars.exception.NullObjectException;
import com.test.cars.repository.ContractRepository;
import com.test.cars.utils.MethodParameters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import static com.test.cars.utils.Constants.NULL_OBJECT;

@Service
public class ContractServiceImpl implements ContractService {

    @Autowired
    private ContractRepository repository;

    @Override
    public Contract create(final ContractDTO contractDTO) {
        Contract newVehicle = null;
        try {
            MethodParameters.testIfTheMethodAtributesAreNotNull(contractDTO);

            newVehicle = ConvertersDTO.getContractFromContractDTO(contractDTO);
        } catch (NullObjectException e) {
            throw new ContractException(NULL_OBJECT, HttpStatus.BAD_REQUEST, e.getMessage());
        }

        return save(newVehicle);
    }

    @Override
    public Contract save(Contract contract) {
        return null;
    }

    @Override
    public Contract update(Contract contract) {
        return null;
    }

    @Override
    public Contract activateContract() {
        return null;
    }

    @Override
    public Contract cancelContract() {
        return null;
    }
}
