package com.test.cars.service;

import com.test.cars.domain.Contract;
import com.test.cars.domain.dto.ContractDTO;

/***
 *
 */
public interface ContractService {

    Contract activateContract();

    Contract cancelContract();

    Contract create(final ContractDTO contractDTO);

    Contract save(final Contract contract);

    Contract update(final Contract contract);
}
