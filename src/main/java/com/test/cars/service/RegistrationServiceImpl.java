package com.test.cars.service;

import com.test.cars.domain.User;
import com.test.cars.domain.dto.ForgotPasswordDTO;
import com.test.cars.exception.NullObjectException;
import com.test.cars.exception.UserException;
import com.test.cars.utils.MethodParameters;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import static com.test.cars.utils.Constants.NULL_OBJECT;

@Service
public class RegistrationServiceImpl implements RegistrationService {

	@Autowired
	private UserService userService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public String changePassword(final ForgotPasswordDTO forgotPasswordDTO) {

		User user = null;

		if (passwordIsValid(forgotPasswordDTO)) {
			user = userService.findByEmail(forgotPasswordDTO.getEmail());
			user.setPassword(passwordEncoder.encode(forgotPasswordDTO.getPassword()));

			userService.update(user);
		} else {
			throw new UserException("The passwords don't match.", HttpStatus.BAD_REQUEST, "The password and confirm password has to be equal.");
		}

		return user.getId();
	}

	private boolean passwordIsValid(final ForgotPasswordDTO forgotPasswordDTO) {

		try {
			MethodParameters.testIfTheMethodAtributesAreNotNull(forgotPasswordDTO);
			if (StringUtils.equals(forgotPasswordDTO.getPassword(), forgotPasswordDTO.getConfirmPassword())) {
				return true;
			}

		} catch (NullObjectException e) {
			throw new UserException(NULL_OBJECT, HttpStatus.BAD_REQUEST, e.getMessage());
		}

		return false;
	}

}