package com.test.cars.service;

import com.test.cars.domain.User;
import com.test.cars.domain.dto.UserRegistrationDTO;

/***
 * 
 * @author sergiu.orian
 *
 */
public interface UserService {
	/**
	 *  Creates a new user.
	 * @param userDTO The input user
	 * @return The 
	 */
	User create(final UserRegistrationDTO userDTO);
	
	User save(final User user);
	
	User update(final User user);

	User findByEmail(final String email);

	String delete(final String id);

	User findById(final String id);

	User findByUsername(final String username);

}