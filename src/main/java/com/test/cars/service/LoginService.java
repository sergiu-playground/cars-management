package com.test.cars.service;

import com.test.cars.domain.User;
import com.test.cars.domain.dto.UserLoginDTO;
import com.test.cars.exception.UserException;
import com.test.cars.utils.Constants;

/***
 * The service for authentication flows.
 */
public interface LoginService {

    /***
     * Authenticate a user.
     *
     * @param userLoginDTO User Login Data transfer object.
     * @return the {@link User} username.
     * @throws UserException with {@link Constants#NULL_OBJECT} error key if the {@link UserLoginDTO} is null
     * @throws UserException with {@link Constants#INVALID_CREDENTIALS} error key if the username or password is incorrect
     */
    String login(final UserLoginDTO userLoginDTO);

    /***
     * Logout the user.
     *
     */
    void logout();

}