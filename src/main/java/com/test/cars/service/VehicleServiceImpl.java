package com.test.cars.service;

import com.test.cars.domain.Branch;
import com.test.cars.domain.Brand;
import com.test.cars.domain.Vehicle;
import com.test.cars.domain.dto.ConvertersDTO;
import com.test.cars.domain.dto.VehicleDTO;
import com.test.cars.exception.NullObjectException;
import com.test.cars.exception.VehicleException;
import com.test.cars.repository.VehicleRepository;
import com.test.cars.utils.MethodParameters;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;

import static com.test.cars.utils.Constants.NULL_OBJECT;

@Service
public class VehicleServiceImpl implements VehicleService {

	@Autowired
	private VehicleRepository repository;

	@Override
	public Vehicle create(final VehicleDTO vehicleDTO) {

		Vehicle newVehicle = null;
		try {
			MethodParameters.testIfTheMethodAtributesAreNotNull(vehicleDTO);

			newVehicle = ConvertersDTO.getVehicleFromVehicleDTO(vehicleDTO);
		} catch (NullObjectException e) {
			throw new VehicleException(NULL_OBJECT, HttpStatus.BAD_REQUEST, e.getMessage());
		}

		return save(newVehicle);
	}

	@Override
	public Vehicle save(final Vehicle vehicle) {

		try {
			MethodParameters.testIfTheMethodAtributesAreNotNull(vehicle);
		} catch (NullObjectException e) {
			throw new VehicleException(NULL_OBJECT, HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return repository.save(vehicle);
	}

	@Override
	public String deleteById(final String id) {

		try {
			MethodParameters.testIfTheMethodAtributesAreNotNull(id);
			findById(id);
			repository.deleteById(id);
		} catch (NullObjectException e) {
			throw new VehicleException(NULL_OBJECT, HttpStatus.BAD_REQUEST, e.getMessage());
		}
		return id;
	}

	@Override
	public Vehicle update(final Vehicle newVehicle) {

		Vehicle oldVehicle = null;
		try {
			MethodParameters.testIfTheMethodAtributesAreNotNull(newVehicle);
			oldVehicle = findById(newVehicle.getVehi_id());
		} catch (NullObjectException e) {
			throw new VehicleException(NULL_OBJECT, HttpStatus.BAD_REQUEST, e.getMessage());
		}

		changeVehicleData(oldVehicle, newVehicle);
		save(oldVehicle);

		return oldVehicle;
	}

	@Override
	public Vehicle findById(final String id) {

		Vehicle vehicle = null;
		try {
			MethodParameters.testIfTheMethodAtributesAreNotNull(id);
			vehicle = repository.findById(id).get();
		} catch (NullObjectException e) {
			throw new VehicleException(NULL_OBJECT, HttpStatus.BAD_REQUEST, e.getMessage());
		} catch (NoSuchElementException e) {
			throw new VehicleException("Vehicle not found.", HttpStatus.NOT_FOUND,
					"The vehicle is not found in database.");

		}

		return vehicle;
	}

	@Override
	public List<Vehicle> findAll() {

		List<Vehicle> vehicles = repository.findAll();

		return vehicles;
	}

	private void changeVehicleData(Vehicle oldVehicle, Vehicle newVehicle) {

		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		oldVehicle.setVehi_modified_by(userDetails.getUsername());
		oldVehicle.setVehi_modified_timestamp(Calendar.getInstance().getTime());

		Branch vehiBranch = newVehicle.getVehi_branch();
		String vehiModelYear = newVehicle.getVehi_model_year();
		Brand vehiBrand = newVehicle.getVehi_brand();
		String vehiVehicleClass = newVehicle.getVehi_vehicle_class();
		String vehiModel = newVehicle.getVehi_model();
		String vehiTransmissionType = newVehicle.getVehi_transmission_type();
		String vehiEngineType = newVehicle.getVehi_engine_type();
		String vehiCommissionNumber = newVehicle.getVehi_commission_number();
		Date vehiRegistrationDate = newVehicle.getVehi_registration_date();
		String vehiEmissionStandard = newVehicle.getVehi_emission_standard();
		String contractId = newVehicle.getContractId();
		String plateNumber = newVehicle.getPlateNumber();

		if (!StringUtils.isEmpty(vehiBranch.getkey())) {
			oldVehicle.setVehi_branch(vehiBranch);
		}
		if (!StringUtils.isEmpty(vehiModelYear)) {
			oldVehicle.setVehi_model_year(vehiModelYear);
		}
		if (!StringUtils.isEmpty(vehiBrand.getKey())) {
			oldVehicle.setVehi_brand(vehiBrand);
		}
		if (!StringUtils.isEmpty(vehiVehicleClass)) {
			oldVehicle.setVehi_vehicle_class(vehiVehicleClass);
		}
		if (!StringUtils.isEmpty(vehiModel)) {
			oldVehicle.setVehi_model(vehiModel);
		}
		if (!StringUtils.isEmpty(vehiTransmissionType)) {
			oldVehicle.setVehi_transmission_type(vehiTransmissionType);
		}
		if (!StringUtils.isEmpty(vehiEngineType)) {
			oldVehicle.setVehi_engine_type(vehiEngineType);
		}
		if (!StringUtils.isEmpty(vehiCommissionNumber)) {
			oldVehicle.setVehi_commission_number(vehiCommissionNumber);
		}
		if (!StringUtils.isEmpty(vehiRegistrationDate.toString())) {
			oldVehicle.setVehi_registration_date(vehiRegistrationDate);
		}
		if (!StringUtils.isEmpty(vehiEmissionStandard)) {
			oldVehicle.setVehi_emission_standard(vehiEmissionStandard);
		}
		if (!StringUtils.isEmpty(contractId)) {
			oldVehicle.setContractId(contractId);
		}
		if (!StringUtils.isEmpty(plateNumber)) {
			oldVehicle.setPlateNumber(plateNumber);
		}

	}

}