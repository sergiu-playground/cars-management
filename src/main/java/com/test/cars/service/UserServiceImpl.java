package com.test.cars.service;

import static com.test.cars.utils.Constants.NULL_OBJECT;

import java.util.NoSuchElementException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.test.cars.domain.User;
import com.test.cars.domain.dto.ConvertersDTO;
import com.test.cars.domain.dto.UserRegistrationDTO;
import com.test.cars.exception.NullObjectException;
import com.test.cars.exception.UserException;
import com.test.cars.repository.UserRepository;
import com.test.cars.utils.MethodParameters;

@Service
public class UserServiceImpl implements UserService {

    public static final String USER_NOT_FOUND = "User not found.";
    @Autowired
    private UserRepository     repository;

    @Autowired
    private PasswordEncoder    passwordEncoder;

    @Override
    public User create(final UserRegistrationDTO userRegistrationDTO) {

        User newUser = null;
        try {
            MethodParameters.testIfTheMethodAtributesAreNotNull(userRegistrationDTO);
            newUser = ConvertersDTO.getUserFromUserRegistrationDTO(userRegistrationDTO);
        } catch (final NullObjectException e) {
            throw new UserException(NULL_OBJECT, HttpStatus.BAD_REQUEST, e.getMessage() + "");
        }
        newUser.setPassword(passwordEncoder.encode(userRegistrationDTO.getPassword()));

        return save(newUser);
    }

    @Override
    public User save(final User user) {

        try {
            MethodParameters.testIfTheMethodAtributesAreNotNull(user);
        } catch (final NullObjectException e) {
            throw new UserException(NULL_OBJECT, HttpStatus.BAD_REQUEST, e.getMessage());
        }

        return repository.save(user);
    }

    @Override
    public User update(final User newUser) {

        User oldUser = null;
        try {
            MethodParameters.testIfTheMethodAtributesAreNotNull(newUser);
            oldUser = findById(newUser.getId());
        } catch (final NullObjectException e) {
            throw new UserException(NULL_OBJECT, HttpStatus.BAD_REQUEST, e.getMessage());
        }

        if (!StringUtils.equals(oldUser.getCNP(), newUser.getCNP())) {
            delete(newUser.getId());
            newUser.setId(newUser.getCNP());
            return save(newUser);

        }

        changeUserData(oldUser, newUser);
        oldUser = save(oldUser);

        return oldUser;

    }

    @Override
    public String delete(final String id) {

        try {
            MethodParameters.testIfTheMethodAtributesAreNotNull(id);
            findById(id);
            repository.deleteById(id);
        } catch (final NullObjectException e) {
            throw new UserException(NULL_OBJECT, HttpStatus.BAD_REQUEST, "The id is not found in database.");
        }
        return id;
    }

    @Override
    public User findByEmail(final String email) {

        User user = null;
        try {
            MethodParameters.testIfTheMethodAtributesAreNotNull(email);
            user = repository.findByEmail(email);
        } catch (final NullObjectException e) {
            throw new UserException(NULL_OBJECT, HttpStatus.BAD_REQUEST, e.getMessage());
        }

        if (user == null) {
            throw new UserException(USER_NOT_FOUND, HttpStatus.NOT_FOUND, "The email is not found in the database.");
        }

        return user;
    }

    @Override
    public User findById(final String id) {

        User user = null;
        try {
            MethodParameters.testIfTheMethodAtributesAreNotNull(id);
            user = repository.findById(id).get();
        } catch (final NullObjectException e) {
            throw new UserException(NULL_OBJECT, HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (final NoSuchElementException e) {
            throw new UserException(USER_NOT_FOUND, HttpStatus.NOT_FOUND, "The user is not found in database.");
        }

        return user;
    }

    @Override
    public User findByUsername(final String username) {
        User user = null;
        try {
            MethodParameters.testIfTheMethodAtributesAreNotNull(username);
            user = repository.findByUsername(username);
        } catch (final NullObjectException e) {
            throw new UserException(NULL_OBJECT, HttpStatus.BAD_REQUEST, e.getMessage());
        }

        if (user == null) {
            throw new UserException(USER_NOT_FOUND, HttpStatus.NOT_FOUND, "The username is not saved in the database.");
        }

        return user;
    }

    private void changeUserData(final User oldUser, final User newUserData) {

        final String newAddres = newUserData.getAddress();
        final String newEmail = newUserData.getEmail();
        final String newName = newUserData.getName();
        final String newPasssord = newUserData.getPassword();
        final String newRole = newUserData.getRole();
        final String newUsername = newUserData.getUsername();

        if (!StringUtils.isEmpty(newAddres)) {
            oldUser.setAddress(newAddres);
        }
        if (!StringUtils.isEmpty(newEmail)) {
            oldUser.setEmail(newEmail);
        }
        if (!StringUtils.isEmpty(newName)) {
            oldUser.setName(newName);
        }
        if (!StringUtils.isEmpty(newPasssord)) {
            oldUser.setPassword(newPasssord);
        }
        if (!StringUtils.isEmpty(newRole)) {
            oldUser.setRole(newRole);
        }
        if (!StringUtils.isEmpty(newUsername)) {
            oldUser.setUsername(newUsername);
        }
    }

}