package com.test.cars.domain;

public enum Branch {
	CAR("0"), TRACK("1"), BUS("2");

	private String key;

	Branch(String key) {
		this.key = key;
	}

	public String getkey() {
		return key;
	}

	public void setkey(String key) {
		this.key = key;
	}

}