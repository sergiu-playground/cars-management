package com.test.cars.domain;

/***
 * 
 * @author sergiu.orian
 *
 */
public enum Portofolio {
	FINANCE_LEASE("1"), OPERATE_LEASE("2"), DIRECT_BUYBACK("3");

	private String key;

	Portofolio(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
}