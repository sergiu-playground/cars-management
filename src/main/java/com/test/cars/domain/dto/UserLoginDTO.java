package com.test.cars.domain.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

@XmlRootElement(name="UserLoginDTO")
public class UserLoginDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6271463102655206462L;

	@NotNull(message = "The email is required.")
	@NotEmpty(message = "The email must not be empty.")
	private String username;

	@NotNull(message = "The password is required.")
	@NotEmpty(message = "The password must not be empty.")
	private String password;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}