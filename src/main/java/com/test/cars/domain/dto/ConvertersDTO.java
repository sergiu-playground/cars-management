package com.test.cars.domain.dto;

import com.test.cars.domain.Contract;
import com.test.cars.domain.User;
import com.test.cars.domain.Vehicle;
import com.test.cars.exception.NullObjectException;
import org.modelmapper.ModelMapper;

import java.util.Calendar;

public class ConvertersDTO {

    private ConvertersDTO() {
    }

    public static User getUserFromUserRegistrationDTO(final UserRegistrationDTO userDTO) throws NullObjectException {



        if (userDTO == null) {
            throw new NullObjectException("The userDTO must not be null.");
        }

        User newUser = new User();

        newUser.setCNP(userDTO.getCNP());
        newUser.setId(userDTO.getCNP()); // CNP is the primary key
        newUser.setAddress(userDTO.getAddress());
        newUser.setEmail(userDTO.getEmail());
        newUser.setName(userDTO.getName());
        newUser.setUsername(userDTO.getUsername());
        newUser.setPassword(userDTO.getPassword());

        return newUser;
    }

    public static Vehicle getVehicleFromVehicleDTO(final VehicleDTO vehicleDTO) throws NullObjectException {
        if (vehicleDTO == null) {
            throw new NullObjectException("The vehicleDTO must not be null.");
        }

        Vehicle newVehicle = new Vehicle();
        newVehicle.setVehi_id(vehicleDTO.getVehi_pin());

        newVehicle.setContractId(vehicleDTO.getContractId());
        newVehicle.setPlateNumber(vehicleDTO.getPlateNumber());
        newVehicle.setVehi_branch(vehicleDTO.getVehi_branch());
        newVehicle.setVehi_brand(vehicleDTO.getVehi_brand());
        newVehicle.setVehi_commission_number(vehicleDTO.getVehi_commission_number());
        newVehicle.setVehi_created_by(vehicleDTO.getVehi_created_by());
        newVehicle.setVehi_created_timestamp(Calendar.getInstance().getTime());
        newVehicle.setVehi_emission_standard(vehicleDTO.getVehi_emission_standard());
        newVehicle.setVehi_engine_type(vehicleDTO.getVehi_engine_type());
        newVehicle.setVehi_model(vehicleDTO.getVehi_model());
        newVehicle.setVehi_model_year(vehicleDTO.getVehi_model_year());
        newVehicle.setVehi_pin(vehicleDTO.getVehi_pin());
        newVehicle.setVehi_registration_date(Calendar.getInstance().getTime());
        newVehicle.setVehi_transmission_type(vehicleDTO.getVehi_transmission_type());
        newVehicle.setVehi_vehicle_class(vehicleDTO.getVehi_vehicle_class());


        return newVehicle;
    }

    public static Contract getContractFromContractDTO(final ContractDTO contractDTO) throws NullObjectException {
        if (contractDTO == null) {
            throw new NullObjectException("The contractDTO must not be null.");
        }

        Contract newContract = null;

        ModelMapper modelMapper = new ModelMapper();
        newContract = modelMapper.map(contractDTO, Contract.class);

        newContract.setContCancellationDate(contractDTO.getContCancellationDate());
        newContract.setContCenterId(contractDTO.getContCenterId());
        newContract.setContContractDuration(contractDTO.getContContractDuration());
        newContract.setContContractRvg(contractDTO.getContContractRvg());
        newContract.setContCreatedBy(contractDTO.getContCreatedBy());

        return newContract;
    }

}
