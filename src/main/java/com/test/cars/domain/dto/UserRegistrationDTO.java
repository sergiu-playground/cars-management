package com.test.cars.domain.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class UserRegistrationDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4271066981949130505L;

	@NotNull(message = "The CNP is required.")
	@NotEmpty(message = "The CNP must not be empty.")
	private String CNP;

	@NotNull(message = "The name is required.")
	@NotEmpty(message = "The name must not be empty.")
	private String name;

	@NotNull(message = "The address is required.")
	@NotEmpty(message = "The address must not be empty.")
	private String address;

	@Email(regexp = "[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", message = "The email format is not good.")
	private String email;
	
	@NotNull(message = "The username is required.")
	@NotEmpty(message = "The username must not be empty.")
	private String username;

	@NotNull(message = "The password is required.")
	@NotEmpty(message = "The password must not be empty.")
	private String password;
	
	@NotNull(message = "The Confirm Password is required.")
	@NotEmpty(message = "The Confirm Password must not be empty.")
	private String confirmPassword;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCNP() {
		return CNP;
	}

	public void setCNP(String cNP) {
		CNP = cNP;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}