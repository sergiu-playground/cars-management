package com.test.cars.domain.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

public class ForgotPasswordDTO {

	@Email(regexp = "[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", message = "The email format is not good.")
	private String email;
	
	@NotNull(message = "The password must not be empty.")
	private String password;
	
	@NotNull(message = "The password must not be empty.")
	private String confirmPassword;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}