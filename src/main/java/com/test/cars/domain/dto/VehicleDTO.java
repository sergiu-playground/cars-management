package com.test.cars.domain.dto;

import com.test.cars.domain.Branch;
import com.test.cars.domain.Brand;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

public class VehicleDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7697470726045441223L;
	
	@NotNull
	@Size(max = 36, message = "The id must contains less than 36 elements.")
	private String vehi_fin;

	@NotNull
	private Branch vehi_branch;

	@NotNull
	@Size(min = 3, max = 3, message = "The year must contains 3 digits.")
	private String vehi_model_year;

	@NotNull
	private Brand vehi_brand;

	@NotNull
	@Size(max = 80, message = "The class of vehicle need to be less than 80 characters.")
	private String vehi_vehicle_class;
	
	@NotNull
	@Size(max = 80, message = "The model need to be less than 80 characters.")
	private String vehi_model;

	@NotNull
	@Size(max = 10, message = "The transmission type need to be less than 10 characters.")
	private String vehi_transmission_type;

	@NotNull
	@Size(max = 10, message = "The engine type need to be less than 10 characters.")
	private String vehi_engine_type;

	@NotNull
	@Size(max = 14, message = "The transmission type need to be less than 14 characters.")
	private String vehi_commission_number;
	
	@NotNull
	@Size(max = 10, message = "The emission standard need to be less than 10 characters.")
	private String vehi_emission_standard;
	
	@NotNull
	private String vehi_created_by;
	
	@NotNull
	private String contractId;
	
	@NotNull
	private String plateNumber;

	public String getVehi_pin() {
		return vehi_fin;
	}

	public void setVehi_pin(String vehi_pin) {
		this.vehi_fin = vehi_pin;
	}

	public Branch getVehi_branch() {
		return vehi_branch;
	}

	public void setVehi_branch(Branch vehi_branch) {
		this.vehi_branch = vehi_branch;
	}

	public String getVehi_model_year() {
		return vehi_model_year;
	}

	public void setVehi_model_year(String vehi_model_year) {
		this.vehi_model_year = vehi_model_year;
	}

	public Brand getVehi_brand() {
		return vehi_brand;
	}

	public void setVehi_brand(Brand vehi_brand) {
		this.vehi_brand = vehi_brand;
	}

	public String getVehi_vehicle_class() {
		return vehi_vehicle_class;
	}

	public void setVehi_vehicle_class(String vehi_vehicle_class) {
		this.vehi_vehicle_class = vehi_vehicle_class;
	}

	public String getVehi_model() {
		return vehi_model;
	}

	public void setVehi_model(String vehi_model) {
		this.vehi_model = vehi_model;
	}

	public String getVehi_transmission_type() {
		return vehi_transmission_type;
	}

	public void setVehi_transmission_type(String vehi_transmission_type) {
		this.vehi_transmission_type = vehi_transmission_type;
	}

	public String getVehi_engine_type() {
		return vehi_engine_type;
	}

	public void setVehi_engine_type(String vehi_engine_type) {
		this.vehi_engine_type = vehi_engine_type;
	}

	public String getVehi_commission_number() {
		return vehi_commission_number;
	}

	public void setVehi_commission_number(String vehi_commission_number) {
		this.vehi_commission_number = vehi_commission_number;
	}

	public String getVehi_emission_standard() {
		return vehi_emission_standard;
	}

	public void setVehi_emission_standard(String vehi_emission_standard) {
		this.vehi_emission_standard = vehi_emission_standard;
	}

	public String getVehi_created_by() {
		return vehi_created_by;
	}

	public void setVehi_created_by(String vehi_created_by) {
		this.vehi_created_by = vehi_created_by;
	}

	public String getContractId() {
		return contractId;
	}

	public void setContractId(String contractId) {
		this.contractId = contractId;
	}

	public String getPlateNumber() {
		return plateNumber;
	}

	public void setPlateNumber(String plateNumber) {
		this.plateNumber = plateNumber;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((contractId == null) ? 0 : contractId.hashCode());
		result = prime * result + ((plateNumber == null) ? 0 : plateNumber.hashCode());
		result = prime * result + ((vehi_branch == null) ? 0 : vehi_branch.hashCode());
		result = prime * result + ((vehi_brand == null) ? 0 : vehi_brand.hashCode());
		result = prime * result + ((vehi_commission_number == null) ? 0 : vehi_commission_number.hashCode());
		result = prime * result + ((vehi_created_by == null) ? 0 : vehi_created_by.hashCode());
		result = prime * result + ((vehi_emission_standard == null) ? 0 : vehi_emission_standard.hashCode());
		result = prime * result + ((vehi_engine_type == null) ? 0 : vehi_engine_type.hashCode());
		result = prime * result + ((vehi_model == null) ? 0 : vehi_model.hashCode());
		result = prime * result + ((vehi_model_year == null) ? 0 : vehi_model_year.hashCode());
		result = prime * result + ((vehi_fin == null) ? 0 : vehi_fin.hashCode());
		result = prime * result + ((vehi_transmission_type == null) ? 0 : vehi_transmission_type.hashCode());
		result = prime * result + ((vehi_vehicle_class == null) ? 0 : vehi_vehicle_class.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VehicleDTO other = (VehicleDTO) obj;
		if (contractId == null) {
			if (other.contractId != null)
				return false;
		} else if (!contractId.equals(other.contractId))
			return false;
		if (plateNumber == null) {
			if (other.plateNumber != null)
				return false;
		} else if (!plateNumber.equals(other.plateNumber))
			return false;
		if (vehi_branch != other.vehi_branch)
			return false;
		if (vehi_brand != other.vehi_brand)
			return false;
		if (vehi_commission_number == null) {
			if (other.vehi_commission_number != null)
				return false;
		} else if (!vehi_commission_number.equals(other.vehi_commission_number))
			return false;
		if (vehi_created_by == null) {
			if (other.vehi_created_by != null)
				return false;
		} else if (!vehi_created_by.equals(other.vehi_created_by))
			return false;
		if (vehi_emission_standard == null) {
			if (other.vehi_emission_standard != null)
				return false;
		} else if (!vehi_emission_standard.equals(other.vehi_emission_standard))
			return false;
		if (vehi_engine_type == null) {
			if (other.vehi_engine_type != null)
				return false;
		} else if (!vehi_engine_type.equals(other.vehi_engine_type))
			return false;
		if (vehi_model == null) {
			if (other.vehi_model != null)
				return false;
		} else if (!vehi_model.equals(other.vehi_model))
			return false;
		if (vehi_model_year == null) {
			if (other.vehi_model_year != null)
				return false;
		} else if (!vehi_model_year.equals(other.vehi_model_year))
			return false;
		if (vehi_fin == null) {
			if (other.vehi_fin != null)
				return false;
		} else if (!vehi_fin.equals(other.vehi_fin))
			return false;
		if (vehi_transmission_type == null) {
			if (other.vehi_transmission_type != null)
				return false;
		} else if (!vehi_transmission_type.equals(other.vehi_transmission_type))
			return false;
		if (vehi_vehicle_class == null) {
			if (other.vehi_vehicle_class != null)
				return false;
		} else if (!vehi_vehicle_class.equals(other.vehi_vehicle_class))
			return false;
		return true;
	}
	
	

}