package com.test.cars.domain;

/***
 * 
 * @author sergiu.orian
 *
 */
public enum Brand {
	BMW("1"), PORSCHE("2");

	private String key;

	Brand(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
}
