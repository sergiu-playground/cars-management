package com.test.cars.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.sql.Date;
import java.util.Objects;

@Document(collection = "contract")
public class Contract {
    @Id
    private String contId;

    @NotNull
    @Size(max = 3,
            message = "The contract id must not be null and the number of characters must be less or equal to 3.")
    private String contCenterId;

    @NotNull
    @Size(min = 12, max = 12,
            message = "The number must contains 12 elements.")
    private String contNumber;

    @NotNull
    @Size(max = 3)
    private ContractState contState;

    @NotNull
    @Size(max = 1)
    private String contVehicleState;

    @NotNull
    @Size(max = 3)
    private Date contCurrentStartDate;

    @NotNull
    private Integer contContractDuration;

    @NotNull
    @Size(max = 1)
    private Portofolio contPortfolio;

    @NotNull
    @Size(min = 2, max = 15)
    private Double contContractRvg; // verify if it's correct

    @NotNull
    @Size(min = 2, max = 15)
    private Double contRateSubvention;

    @NotNull
    private Integer contMileagePerYear;

    @NotNull
    @Size(min = 4, max = 4)
    private String contRuGuarantor;

    @NotNull
    private String contCreatedBy;

    @NotNull
    private Date contCreatedTimestamp;

    @NotNull
    private String contModifiedBy;

    @NotNull
    private Date contModifiedTimestamp;

    @NotNull
    private Integer contVersion;

    @NotNull
    private Date contCancellationDate;

    @NotNull
    private String vehicleId;

    public String getContId() {
        return contId;
    }

    public void setContId(String contId) {
        this.contId = contId;
    }

    public String getContCenterId() {
        return contCenterId;
    }

    public void setContCenterId(String contCenterId) {
        this.contCenterId = contCenterId;
    }

    public String getContNumber() {
        return contNumber;
    }

    public void setContNumber(String contNumber) {
        this.contNumber = contNumber;
    }

    public ContractState getContState() {
        return contState;
    }

    public void setContState(ContractState contState) {
        this.contState = contState;
    }

    public String getContVehicleState() {
        return contVehicleState;
    }

    public void setContVehicleState(String contVehicleState) {
        this.contVehicleState = contVehicleState;
    }

    public Date getContCurrentStartDate() {
        return contCurrentStartDate;
    }

    public void setContCurrentStartDate(Date contCurrentStartDate) {
        this.contCurrentStartDate = contCurrentStartDate;
    }

    public Integer getContContractDuration() {
        return contContractDuration;
    }

    public void setContContractDuration(Integer contContractDuration) {
        this.contContractDuration = contContractDuration;
    }

    public Portofolio getContPortfolio() {
        return contPortfolio;
    }

    public void setContPortfolio(Portofolio contPortfolio) {
        this.contPortfolio = contPortfolio;
    }

    public Double getContContractRvg() {
        return contContractRvg;
    }

    public void setContContractRvg(Double contContractRvg) {
        this.contContractRvg = contContractRvg;
    }

    public Double getContRateSubvention() {
        return contRateSubvention;
    }

    public void setContRateSubvention(Double contRateSubvention) {
        this.contRateSubvention = contRateSubvention;
    }

    public Integer getContMileagePerYear() {
        return contMileagePerYear;
    }

    public void setContMileagePerYear(Integer contMileagePerYear) {
        this.contMileagePerYear = contMileagePerYear;
    }

    public String getContRuGuarantor() {
        return contRuGuarantor;
    }

    public void setContRuGuarantor(String contRuGuarantor) {
        this.contRuGuarantor = contRuGuarantor;
    }

    public String getContCreatedBy() {
        return contCreatedBy;
    }

    public void setContCreatedBy(String contCreatedBy) {
        this.contCreatedBy = contCreatedBy;
    }

    public Date getContCreatedTimestamp() {
        return contCreatedTimestamp;
    }

    public void setContCreatedTimestamp(Date contCreatedTimestamp) {
        this.contCreatedTimestamp = contCreatedTimestamp;
    }

    public String getContModifiedBy() {
        return contModifiedBy;
    }

    public void setContModifiedBy(String contModifiedBy) {
        this.contModifiedBy = contModifiedBy;
    }

    public Date getContModifiedTimestamp() {
        return contModifiedTimestamp;
    }

    public void setContModifiedTimestamp(Date contModifiedTimestamp) {
        this.contModifiedTimestamp = contModifiedTimestamp;
    }

    public Integer getContVersion() {
        return contVersion;
    }

    public void setContVersion(Integer contVersion) {
        this.contVersion = contVersion;
    }

    public Date getContCancellationDate() {
        return contCancellationDate;
    }

    public void setContCancellationDate(Date contCancellationDate) {
        this.contCancellationDate = contCancellationDate;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Contract contract = (Contract) o;
        return Objects.equals(contId, contract.contId) &&
                Objects.equals(contCenterId, contract.contCenterId) &&
                Objects.equals(contNumber, contract.contNumber) &&
                contState == contract.contState &&
                Objects.equals(contVehicleState, contract.contVehicleState) &&
                Objects.equals(contCurrentStartDate, contract.contCurrentStartDate) &&
                Objects.equals(contContractDuration, contract.contContractDuration) &&
                contPortfolio == contract.contPortfolio &&
                Objects.equals(contContractRvg, contract.contContractRvg) &&
                Objects.equals(contRateSubvention, contract.contRateSubvention) &&
                Objects.equals(contMileagePerYear, contract.contMileagePerYear) &&
                Objects.equals(contRuGuarantor, contract.contRuGuarantor) &&
                Objects.equals(contCreatedBy, contract.contCreatedBy) &&
                Objects.equals(contCreatedTimestamp, contract.contCreatedTimestamp) &&
                Objects.equals(contModifiedBy, contract.contModifiedBy) &&
                Objects.equals(contModifiedTimestamp, contract.contModifiedTimestamp) &&
                Objects.equals(contVersion, contract.contVersion) &&
                Objects.equals(contCancellationDate, contract.contCancellationDate) &&
                Objects.equals(vehicleId, contract.vehicleId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(contId, contCenterId, contNumber, contState, contVehicleState, contCurrentStartDate, contContractDuration, contPortfolio, contContractRvg, contRateSubvention, contMileagePerYear, contRuGuarantor, contCreatedBy, contCreatedTimestamp, contModifiedBy, contModifiedTimestamp, contVersion, contCancellationDate, vehicleId);
    }
}
