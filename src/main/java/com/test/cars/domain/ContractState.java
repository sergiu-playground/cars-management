package com.test.cars.domain;

public enum ContractState {

	IN_PREPARATION("BIP"), ACTIVE("BAC"), CANCELED("BCA");

	private String key;

	ContractState(String key) {
		this.key = key;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

}