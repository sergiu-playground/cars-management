package com.test.cars;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
@JsonInclude
public class FirstPartApplication {

	public static void main(String[] args) {
		SpringApplication.run(FirstPartApplication.class, args);
	}

}