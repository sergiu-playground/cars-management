package com.test.cars.security;

import com.test.cars.domain.User;
import com.test.cars.exception.NullObjectException;
import com.test.cars.exception.UserException;
import com.test.cars.service.UserService;
import com.test.cars.utils.MethodParameters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;

import static com.test.cars.utils.Constants.NULL_OBJECT;

/***
 * 
 * @author sergiu.orian
 *
 */
@Service
public class CustomUserDetailsService implements UserDetailsService {

	@Autowired
	private UserService userService;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		User user = null;

		try {
			MethodParameters.testIfTheMethodAtributesAreNotNull(username);
			user = userService.findByUsername(username);
		} catch (NullObjectException e) {
			throw new UserException(NULL_OBJECT, HttpStatus.BAD_REQUEST, e.getMessage());
		} catch (UserException e) {
			throw new UserException("Invalid credentials.", HttpStatus.NOT_FOUND,
					"Username or passward is incorrect.");
		}

		Collection<GrantedAuthority> grantedAuthorities = new ArrayList<>();
		grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_" + user.getRole()));

		return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), true, true,
				true, true, grantedAuthorities);
	}

}
