package com.test.cars.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable().authorizeRequests().antMatchers("/api/auth/login", "/api/registration/**").permitAll()
				.antMatchers("/api/vehicle/add", "/api/vehicle/find/**", "/api/vehicle/findAll", 
						"/api/contract/**", 
						"/api/user/**", 
						"/api/auth/logout").authenticated()
				.antMatchers("/api/vehicle/delete/**", "/api/vehicle/update", "/api/vehicle/updateLicensePlate").hasRole("ADMIN");

	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

}