package com.test.cars.controller;

import com.test.cars.domain.Contract;
import com.test.cars.domain.dto.ContractDTO;
import com.test.cars.exception.ContractException;
import com.test.cars.service.ContractService;
import com.test.cars.utils.ResponseBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api/contract")
public class ContractController {

    @Autowired
    private ContractService service;

    @RequestMapping(value = "/add", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE,
            MediaType.APPLICATION_XML_VALUE }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ResponseBuilder> add(@Valid @RequestBody ContractDTO contractDTO){

        ResponseBuilder response = new ResponseBuilder();
        try {
            Contract contract = service.create(contractDTO);
            response.setData(contract);
            response.setSuccess(true);
            response.setStatusCode(HttpStatus.OK);

        } catch (ContractException e) {
            response.setSuccess(true);
            response.setError(e.getArguments());
            response.setStatusCode(e.getStatus());
        }

        return new ResponseEntity<>(response, response.getStatusCode());

    }

}
