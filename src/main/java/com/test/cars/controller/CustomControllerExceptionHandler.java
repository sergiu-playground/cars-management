package com.test.cars.controller;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.test.cars.utils.ResponseBuilder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.xml.bind.UnmarshalException;

@ControllerAdvice
public class CustomControllerExceptionHandler {

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<ResponseBuilder> handleException(HttpMessageNotReadableException ex) {
        ResponseBuilder responseBuilder = new ResponseBuilder();
        String error = null;

        if (ex.getCause() instanceof UnmarshalException) {
            if (StringUtils.isNotEmpty(ex.getCause().getMessage())) {
                error = ex.getCause().getMessage();
            } else {
                error = ((UnmarshalException) ex.getCause()).getLinkedException().getMessage();
            }
        } else if (ex.getCause() instanceof InvalidFormatException) {
            JsonMappingException jme = (JsonMappingException) ex.getCause();
            error = jme.getPath().get(0).getFieldName() + " invalid";
        }

        responseBuilder.setError(error);
        responseBuilder.setStatusCode(HttpStatus.BAD_REQUEST);
        responseBuilder.setSuccess(true);
        return new ResponseEntity<>(responseBuilder, responseBuilder.getStatusCode());
    }
}