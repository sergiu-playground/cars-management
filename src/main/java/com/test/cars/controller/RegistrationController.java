package com.test.cars.controller;

import com.test.cars.domain.dto.ForgotPasswordDTO;
import com.test.cars.domain.dto.UserRegistrationDTO;
import com.test.cars.exception.UserException;
import com.test.cars.service.RegistrationServiceImpl;
import com.test.cars.service.UserService;
import com.test.cars.utils.ResponseBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api/registration")
public class RegistrationController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private RegistrationServiceImpl registratonService;

	@ExceptionHandler
	@RequestMapping(value = "/register", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_UTF8_VALUE,
			MediaType.APPLICATION_XML_VALUE }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<ResponseBuilder> register(@Valid @RequestBody UserRegistrationDTO userDTO) {

		ResponseBuilder response = new ResponseBuilder();

		try {
			userService.create(userDTO);
			response.setSuccess(true);
			response.setStatusCode(HttpStatus.OK);

		} catch (UserException e) {
			response.setSuccess(true);
			response.setError(e.getArguments());
			response.setStatusCode(e.getStatus());
		}

		return new ResponseEntity<>(response, response.getStatusCode());
	}

	@RequestMapping(value = "/forgotPassword", method = RequestMethod.PUT, consumes = {
			MediaType.APPLICATION_JSON_UTF8_VALUE,
			MediaType.APPLICATION_XML_VALUE }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<ResponseBuilder> forgotPassword(@Valid @RequestBody ForgotPasswordDTO forgotPasswordDTO) {
		
		ResponseBuilder response = new ResponseBuilder();
		
		try {
			String userId = registratonService.changePassword(forgotPasswordDTO);
			response.setData(userId);
			response.setSuccess(true);
			response.setStatusCode(HttpStatus.OK);
		} catch (UserException e) {
			response.setSuccess(true);
			response.setError(e.getArguments());
			response.setStatusCode(e.getStatus());
		}
		
		return new ResponseEntity<ResponseBuilder>(response, response.getStatusCode());
	}

}