package com.test.cars.controller;

import com.test.cars.domain.dto.UserLoginDTO;
import com.test.cars.exception.UserException;
import com.test.cars.service.LoginService;
import com.test.cars.utils.ResponseBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/auth")
public class LoginController {

	@Autowired
	private LoginService loginService;

	@RequestMapping(value = "/login", method = RequestMethod.POST, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE,
			MediaType.APPLICATION_XML_VALUE }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<ResponseBuilder> login(@Valid @RequestBody UserLoginDTO userLoginDTO) {
		ResponseBuilder response = new ResponseBuilder();
		try {
			String userId = loginService.login(userLoginDTO);
			response.setData(userId);
			response.setSuccess(true);
			response.setStatusCode(HttpStatus.OK);

		} catch (UserException e) {
			response.setSuccess(true);
			response.setError(e.getArguments());
			response.setStatusCode(e.getStatus());
		}

		return new ResponseEntity<ResponseBuilder>(response, response.getStatusCode());
	}

	@RequestMapping(value = "/logout", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<ResponseBuilder> logout() {
		ResponseBuilder response = new ResponseBuilder();
		try {
			loginService.logout();
			response.setSuccess(true);
			response.setStatusCode(HttpStatus.OK);

		} catch (UserException e) {
			response.setSuccess(true);
			response.setError(e.getArguments());
			response.setStatusCode(e.getStatus());
		}

		return new ResponseEntity<ResponseBuilder>(response, response.getStatusCode());
	}

}