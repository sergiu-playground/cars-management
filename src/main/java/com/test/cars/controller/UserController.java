package com.test.cars.controller;

import com.test.cars.domain.User;
import com.test.cars.exception.UserException;
import com.test.cars.service.UserService;
import com.test.cars.utils.ResponseBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api/user")
public class UserController {

	@Autowired
	private UserService service;

	@RequestMapping(value = "/update", method = RequestMethod.PUT, consumes = { MediaType.APPLICATION_JSON_UTF8_VALUE,
			MediaType.APPLICATION_XML_VALUE }, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<ResponseBuilder> updateUserData(@Valid @RequestBody User user) {

		ResponseBuilder response = new ResponseBuilder();
		try {
			User newUser = service.update(user);
			response.setData(newUser);
			response.setSuccess(true);
			response.setStatusCode(HttpStatus.OK);

		} catch (UserException e) {
			response.setSuccess(true);
			response.setError(e.getArguments());
			response.setStatusCode(e.getStatus());
		}

		return new ResponseEntity<ResponseBuilder>(response, response.getStatusCode());
	}

}