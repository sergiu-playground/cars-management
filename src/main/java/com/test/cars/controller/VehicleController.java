package com.test.cars.controller;


import com.test.cars.domain.Vehicle;
import com.test.cars.domain.dto.VehicleDTO;
import com.test.cars.exception.VehicleException;
import com.test.cars.service.VehicleService;
import com.test.cars.utils.ResponseBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/***
 *
 */
@RestController
@RequestMapping(value = "/api/vehicle")
public class VehicleController {

    @Autowired
    private VehicleService service;

    /***
     *
     * @param vehicleDTO
     * @return
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE,
            MediaType.APPLICATION_XML_VALUE}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ResponseBuilder> addVehicle(@Valid @RequestBody VehicleDTO vehicleDTO) {
        ResponseBuilder response = new ResponseBuilder();
        try {
            Vehicle newVehicle = service.create(vehicleDTO);
            response.setData(newVehicle);
            response.setSuccess(true);
            response.setStatusCode(HttpStatus.OK);

        } catch (VehicleException e) {
            response.setSuccess(true);
            response.setError(e.getArguments());
            response.setStatusCode(e.getStatus());
        }

        return new ResponseEntity<ResponseBuilder>(response, response.getStatusCode());

    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ResponseBuilder> deleteVehicle(@NotNull @PathVariable("id") String id) {
        ResponseBuilder response = new ResponseBuilder();
        try {
            String oldVehicleId = service.deleteById(id);
            response.setData(oldVehicleId);
            response.setSuccess(true);
            response.setStatusCode(HttpStatus.OK);

        } catch (VehicleException e) {
            response.setSuccess(true);
            response.setError(e.getArguments());
            response.setStatusCode(e.getStatus());
        }

        return new ResponseEntity<ResponseBuilder>(response, response.getStatusCode());

    }

    @RequestMapping(value = "/findAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ResponseBuilder> findAll() {
        ResponseBuilder response = new ResponseBuilder();
        try {
            List<Vehicle> oldVehicleId = service.findAll();
            response.setData(oldVehicleId);
            response.setSuccess(true);
            response.setStatusCode(HttpStatus.OK);

        } catch (VehicleException e) {
            response.setSuccess(true);
            response.setError(e.getArguments());
            response.setStatusCode(e.getStatus());
        }

        return new ResponseEntity<ResponseBuilder>(response, response.getStatusCode());

    }

    @RequestMapping(value = "/find/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ResponseBuilder> findById(@NotNull @PathVariable("id") String id) {
        ResponseBuilder response = new ResponseBuilder();
        try {
            Vehicle oldVehicleId = service.findById(id);
            response.setData(oldVehicleId);
            response.setSuccess(true);
            response.setStatusCode(HttpStatus.OK);

        } catch (VehicleException e) {
            response.setSuccess(true);
            response.setError(e.getArguments());
            response.setStatusCode(e.getStatus());
        }

        return new ResponseEntity<ResponseBuilder>(response, response.getStatusCode());

    }

    @RequestMapping(value = "/update", method = RequestMethod.PUT, consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE,
            MediaType.APPLICATION_XML_VALUE}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ResponseBuilder> update(@Valid @RequestBody Vehicle vehicle) {
        ResponseBuilder response = new ResponseBuilder();
        try {
            Vehicle oldVehicleId = service.update(vehicle);
            response.setData(oldVehicleId);
            response.setSuccess(true);
            response.setStatusCode(HttpStatus.OK);

        } catch (VehicleException e) {
            response.setSuccess(true);
            response.setError(e.getArguments());
            response.setStatusCode(e.getStatus());
        }

        return new ResponseEntity<ResponseBuilder>(response, response.getStatusCode());

    }

    @RequestMapping(value = "/updateLicensePlate", method = RequestMethod.PATCH, consumes = {MediaType.APPLICATION_JSON_UTF8_VALUE,
            MediaType.APPLICATION_XML_VALUE}, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ResponseBuilder> update(@Valid @RequestParam String licensePlate) {
        ResponseBuilder response = new ResponseBuilder();
        try {
            Vehicle newVehicle = new Vehicle();
            newVehicle.setPlateNumber(licensePlate);

            Vehicle oldVehicleId = service.update(newVehicle);
            response.setData(oldVehicleId);
            response.setSuccess(true);
            response.setStatusCode(HttpStatus.OK);

        } catch (VehicleException e) {
            response.setSuccess(true);
            response.setError(e.getArguments());
            response.setStatusCode(e.getStatus());
        }

        return new ResponseEntity<ResponseBuilder>(response, response.getStatusCode());

    }

}