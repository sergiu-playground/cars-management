package service;

import com.test.cars.domain.User;
import com.test.cars.domain.dto.UserRegistrationDTO;
import com.test.cars.repository.UserRepository;
import com.test.cars.service.UserServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.MockitoAnnotations.initMocks;

public class UserServiceTest {
	
	private static final String ADDRESS = "Str. Unirii No. 106";
	private static final String CNP = "1234567890123";
	private static final String PASSWORD = "abc123";
	private static final String CONFIRM_PASSWORD = "abc123";
	private static final String EMAIL = "good.email@test.com";
	private static final String NAME = "Jon Snow";
	private static final String USERNAME = "jon.snow";

	@InjectMocks
	private UserServiceImpl victim;
	
	@Mock
	private UserRepository userRepository;
	
	@Mock
	private PasswordEncoder passwordEncoder;
	
	@Before
	public void setUp() {
		initMocks(this);
	}
	
	@Test
	public void shouldCreateAUser() {
		UserRegistrationDTO userRegistrationDTO = createUserRegistrationDTO();
		User user = createUser();
		
		doReturn(user).when(userRepository).save(any(User.class));
		
		User expected = user;
		User result = victim.create(userRegistrationDTO);
		
		assertEquals(expected, result);
		
	}

	private User createUser() {
		User user = new User();
		user.setAddress(ADDRESS);
		user.setCNP(CNP);
		user.setEmail(EMAIL);
		user.setName(NAME);
		user.setPassword(PASSWORD);
		user.setUsername(USERNAME);
		
		return user;
	}

	private UserRegistrationDTO createUserRegistrationDTO() {
		UserRegistrationDTO userRegistrationDTO = new UserRegistrationDTO();
		
		userRegistrationDTO.setAddress(ADDRESS);
		userRegistrationDTO.setCNP(CNP);
		userRegistrationDTO.setConfirmPassword(CONFIRM_PASSWORD);
		userRegistrationDTO.setPassword(PASSWORD);
		userRegistrationDTO.setEmail(EMAIL);
		userRegistrationDTO.setName(NAME);
		userRegistrationDTO.setUsername(USERNAME);
		
		return userRegistrationDTO;
	}
	

}
